package SleepingBarber;

import static SleepingBarber.Sleep.sleep;

public class Main {
    private static final int NUMBER_OF_CHAIRS = 5;
    private static final int MIN_TIME_FOR_ARRIVAL_OF_CLIENT = 2000;
    private static final int TIME_FOR_ARRIVAL_OF_CLIENT_INTERVAL = 5000;

    public static void main(String[] args) {
        Chair chair = new Chair();
        Queue queue = new Queue(NUMBER_OF_CHAIRS);
        Barber barber = new Barber(queue, chair);
        (new Thread(barber)).start();
        while (true) {
            (new Thread(new Customer(barber, queue))).start();
            sleep(MIN_TIME_FOR_ARRIVAL_OF_CLIENT, TIME_FOR_ARRIVAL_OF_CLIENT_INTERVAL);
        }
    }
}

