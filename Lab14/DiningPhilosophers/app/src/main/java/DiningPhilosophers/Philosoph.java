package DiningPhilosophers;

public class Philosoph implements Runnable{

    private static final int MIN_TIME_FOR_MEDITATES = 3000;

    private static final int TIME_FOR_MEDITATES_INTERVAL = 3000;

    private static final int MIN_TIME_FOR_EAT = 3000;

    private static final int TIME_FOR_EAT_INTERVAL = 2000;

    public Waiter waiter;

    public PStatus status = PStatus.THINK;

    Fork rightFork;

    Fork leftFork;

    private String name;

    Philosoph(String name, Fork leftFork, Fork rightFork, Waiter waiter){
        this.name = name;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        this.waiter = waiter;
    }

    @Override
    public void run() {
        while (true) {
            try {
                this.waiter.accessToFork();
                if (this.leftFork.forkIsBusy() && this.rightFork.forkIsBusy()) {
                    Thread.sleep(MIN_TIME_FOR_MEDITATES + (int)(Math.random() * TIME_FOR_MEDITATES_INTERVAL));
                    this.leftFork.takeFork();
                    System.out.println(this.name + " взял левую вилку");

                    Thread.sleep(MIN_TIME_FOR_MEDITATES + (int)(Math.random() * TIME_FOR_MEDITATES_INTERVAL));
                    this.rightFork.takeFork();
                    System.out.println(this.name + " взял правую вилку");

                    System.out.println(this.name + " начал трапезу");
                    this.status = PStatus.EAT;
                    Thread.sleep(MIN_TIME_FOR_EAT + (int)(Math.random() * TIME_FOR_EAT_INTERVAL));
                    System.out.println(this.name + " закончил кушать лапшу");

                    Thread.sleep(MIN_TIME_FOR_MEDITATES + (int)(Math.random() * TIME_FOR_MEDITATES_INTERVAL));
                    this.leftFork.putFork();
                    System.out.println(this.name + " положил левую вилку");

                    Thread.sleep(MIN_TIME_FOR_MEDITATES + (int)(Math.random() * TIME_FOR_MEDITATES_INTERVAL));
                    this.rightFork.putFork();
                    System.out.println(this.name + " положил правую вилку");

                    System.out.println(this.name + " начал думать");
                    this.status = PStatus.THINK;
                }
                this.waiter.giveFork();
            } catch (InterruptedException e) {}
        }
    }

}


