package Num3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LikeWord {
    public static void main(String[] args) {
        String str = "From fairest creatures we desire increase,\n" +
                "That thereby beauty's rose might never die";
        String[] unloved = new String[]{"rose"};
        String favorite = "daisies";
        System.out.println(checkText(str, unloved, favorite));
    }
    public static String checkText(String text, String[] unloved, String favorite) {
        String joined = String.join("\\b|\\b", unloved);
        joined = "\\b" + joined + "\\b";
        Pattern pattern = Pattern.compile(joined);
        Matcher matcher = pattern.matcher(text);
        text = matcher.replaceAll(favorite);
        return text;
    }
}

