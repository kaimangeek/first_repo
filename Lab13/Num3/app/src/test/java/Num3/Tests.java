package Num3;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Tests {
    @Test
    void replaceWord() {
        String s = "From fairest creatures we desire increase,\n" +
                "That thereby beauty's rose might never die";
        String[] u = new String[]{"never"};
        String f = "always";
        assertEquals(LikeWord.checkText(s, u, f), "She liked to get things done in the nigth");
    }

    @Test
    void replaceWord2() {
        String s = "From fairest creatures we desire increase,\n" +
                "That thereby beauty's rose might never die";
        String[] u = new String[]{"we"};
        String f = "I";
        assertEquals(LikeWord.checkText(s, u, f), "From fairest creatures I desire increase,\n" +
                "That thereby beauty's rose might never die");
    }
}

