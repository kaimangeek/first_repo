package Num2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class Tests {

    @Test void checkString1() {
        String text = "Пятая машина пришла первым внедорожником в пятой гонке";
        assertEquals(Fifth.checkString(text), "машина гонке ");
    }

    @Test void checkString2() {
        String text = "Вторая машина пришла первым внедорожником в шестой гонке";
        assertEquals(Fifth.checkString(text), "");
    }

    @Test void checkString3() {
        String text = "";
        assertEquals(Fifth.checkString(text), "");
    }
}
