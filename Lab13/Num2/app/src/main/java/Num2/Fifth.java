package Num2;

import java.util.regex.Matcher;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.Scanner;


public class Fifth {
    public static void main(String[] args) {
        System.out.println("Введите строку: ");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println(checkString(text));
    }
    public static String checkString(String text) {
        String regexStr = "(^|\\s)[пП]ят(ый|ая|ое|ые|ого|ому|ом|ой|ую|ою|ых|ым|ыми)\\s+(\\w+)";
        Pattern pattern = Pattern.compile(regexStr, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(text);
        String result = "";
        while (matcher.find()) {
            MatchResult matchresult = matcher.toMatchResult();
            result += matchresult.group(3) + " ";
        }
        return result;
    }
}
