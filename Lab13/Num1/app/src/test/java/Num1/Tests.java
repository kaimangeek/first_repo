package Num1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {

    @Test
    void checkPhone1() {
        String number = "89654752988";
        assertTrue(PhoneNumber.checkPhone(number));
    }

    @Test
    void checkPhone2() {
        String number = "8(965)475-29-88";
        assertTrue(PhoneNumber.checkPhone(number));
    }

    @Test
    void checkPhone3() {
        String number = "8-965-475-29-88";
        assertTrue(PhoneNumber.checkPhone(number));
    }

    @Test
    void checkPhone4() {
        String number = "+7-(965)-475-29-88";
        assertTrue(PhoneNumber.checkPhone(number));
    }
}

