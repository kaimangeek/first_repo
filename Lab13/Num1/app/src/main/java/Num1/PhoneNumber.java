package Num1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;


public class PhoneNumber {
    public static void main(String[] args) {
        System.out.println("Введите номер телефона");
        Scanner scanner = new Scanner(System.in);
        String phone = scanner.nextLine();
        System.out.println(checkPhone(phone));
    }
    public static boolean checkPhone(String phone) {
        String regexPhone = "^\\+?[1-9]{1,2}(\\-|\\s)?(\\d{3}|\\(\\d{3}\\))(\\-|\\s)?\\d{3}((\\-|\\s)?\\d{2}){2}";
        Pattern pattern = Pattern.compile(regexPhone);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}