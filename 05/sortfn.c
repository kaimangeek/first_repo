#include <stdio.h>

int a[5] = {4, 2, 7, 5, 9};
int n = 5;

int mass() {
    for(int i = 0 ; i < n - 1; i++) { 
        if(a[i] > a[i+1]) {           
            int mem = a[i];
            a[i] = a[i+1] ;
            a[i+1] = mem; 
        }
    }  
}

int ress(void) {
    for(int j = 0; j < n; ++j) {
        printf("%d ", a[j]);
     }
}

int main() {
    mass();
    ress();
}
