/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package workers;

public class Main{

  public static void main(String[] args){
    int n = 5;
    Worker[] workers = new Worker[n];
    workers[0] = new Accountant("Дмитрий", "Бухгалтер", 40000);
    workers[1] = new MainAccountant ("Александр", "Главный бухгалтер", 50000);
    workers[2] = new Engineer ("Сергей", "Инженер", 60000);
    workers[3] = new Workman ("Семен", "Рабочий", 25000);
    workers[4] = new WorkerInGeneral ("Матвей", "Работник", 35000);

    for (int i = 0; i < workers.length; i++){
      System.out.println(workers[i]);
    }

  }


}
