package Num2;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Level;
import java.util.Map;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

@State(Scope.Benchmark)
public class BenchmarkTest {
    public static final int ARRAY_SIZE = 30000;
    Maps mapOperations;
    Map<String, Integer> map;
    HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
    LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<String, Integer>();
    TreeMap<String, Integer> treeMap = new TreeMap<String, Integer>();
    Hashtable<String, Integer> hashTable = new Hashtable<String, Integer>();
    String[] arrayHashMap = new String[ARRAY_SIZE];
    String[] arrayLinkedHashMap = new String[ARRAY_SIZE];
    String[] arrayTreeMap = new String[ARRAY_SIZE];
    String[] arrayHashTable = new String[ARRAY_SIZE];
    int sum = 0;

    public BenchmarkTest() {
        mapOperations = new Maps();
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationHashMap() {
        map = mapOperations.generateMap(hashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationArrayHashMap() {
        arrayHashMap = mapOperations.generateArray(arrayHashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSumNumbersHashMap() {
        sum = mapOperations.sumNumbers(arrayHashMap, map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationLinkedHashMap() {
        map = mapOperations.generateMap(linkedHashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationArrayLinkedHashMap() {
        arrayLinkedHashMap = mapOperations.generateArray(arrayLinkedHashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSumNumbersLinkedHashMap() {
        sum = mapOperations.sumNumbers(arrayLinkedHashMap, map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationTreeMap() {
        map = mapOperations.generateMap(treeMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationArrayTreeMap() {
        arrayTreeMap = mapOperations.generateArray(arrayTreeMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSumNumbersTreeMap() {
        sum = mapOperations.sumNumbers(arrayTreeMap, map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationHashTable() {
        map = mapOperations.generateMap(hashTable);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationArrayHashTable() {
        arrayHashTable = mapOperations.generateArray(arrayHashTable);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSumNumbersHashTable() {
        sum = mapOperations.sumNumbers(arrayHashTable, map);
    }
}
