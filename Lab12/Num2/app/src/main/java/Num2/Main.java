package Num2;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;
import java.lang.InterruptedException;

public class Main {
    public static final int ARRAY_SIZE = 30000;
    public static final int MILLISECONDS_IN_SECOND = 1000;

    public static void main(String[] args) throws InterruptedException {

        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        mapTestTime(hashMap);

        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<String, Integer>();
        mapTestTime(linkedHashMap);

        TreeMap<String, Integer> treeMap = new TreeMap<String, Integer>();
        mapTestTime(treeMap);

        Hashtable<String, Integer> hashTable = new Hashtable<String, Integer>();
        mapTestTime(hashTable);

    }

    public static void mapTestTime(Map<String, Integer> map) {
        double startAllTime = System.currentTimeMillis();

        Maps mapOperations = new Maps();
        String[] array = new String[ARRAY_SIZE];
        int sum = 0;

        double startGenerationMapTime = System.currentTimeMillis();

        map = mapOperations.generateMap(map);

        double elapsedGenerationMapTime = getTime(startGenerationMapTime);

        double startGenerationArrayTime = System.currentTimeMillis();

        array = mapOperations.generateArray(array);

        double elapsedGenerationArrayTime = getTime(startGenerationArrayTime);

        double startSumNumbersTime = System.currentTimeMillis();

        sum = mapOperations.sumNumbers(array, map);

       double elapsedSumNumbersTime = getTime(startSumNumbersTime);

        double finishAllTime = System.currentTimeMillis();
        double elapsedAllTime = (finishAllTime - startAllTime) / MILLISECONDS_IN_SECOND;
        System.out.println("GenerationMap: " + elapsedGenerationMapTime + "секунд");
        System.out.println("GenerationArray: " + elapsedGenerationArrayTime + "секунд");
        System.out.println("SumNumbers: " + elapsedSumNumbersTime + "секунд");
        System.out.println("All: " + elapsedAllTime + "секунд");
        System.out.println("Sum = " + sum);
    }
    private static double getTime(double num) {
        double finishGenerationMapTime = System.currentTimeMillis();
        double elapsedGenerationMapTime = (finishGenerationMapTime - num) / MILLISECONDS_IN_SECOND;
        return elapsedGenerationMapTime;
    }
}
