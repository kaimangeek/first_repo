package Num2;

import java.util.Random;
import java.util.Map;

public class Maps {
    public static final int MAXVALUE = 2000000;
    public static final int SIZE_STRING = 10;
    public Map<String, Integer> generateMap(Map<String, Integer> map) {
        for (int i = 0; i < MAXVALUE; i++) {
            int value = (int) (Math.random() * MAXVALUE);
            map.put(generateString(), value);
        }
        return map;
    }
    public String[] generateArray(String[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = generateString();
        }
        return array;
    }
    public int sumNumbers(String[] array, Map<String, Integer> map) {
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            Integer value = map.get(array[i]);
            if (value != null) {
                sum += value;
            }
        }
        return sum;
    }
    private static String generateString() {
        String name = "";
        Random r = new Random();
        for (int i = 0; i < SIZE_STRING; i++) {
            char c = (char) (r.nextInt(26) + 'a');
            name += c;
        }
        return name;
    }
}
