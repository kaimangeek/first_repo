package Num1;

import java.util.Random;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;


public class Maps {
    public static final int MAXVAL = 2000000;
    public static final int SIZE_STRING = 10;

    public Map<String, Integer> generationMap(Map<String, Integer> map) {
        for (int i = 0; i < MAXVAL; i++) {
            int value = (int) (Math.random() * MAXVAL);
            map.put(generateKey(), value);
        }

        return map;
    }

    public ArrayList<Integer> putList(Map<String, Integer> map) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (Map.Entry<String, Integer> item : map.entrySet()) {
            arrayList.add(item.getValue());
        }
        return arrayList;
    }

    public ArrayList<Integer> sortingList(ArrayList<Integer> arrayList) {
        Collections.sort(arrayList);
        return arrayList;
    }

    public void outputArrayList(ArrayList<Integer> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.print(arrayList.get(i) + ", ");
        }
    }

    private static String generateKey() {
        String name = "";
        Random r = new Random();
        for (int i = 0; i < SIZE_STRING; i++) {
            char c = (char) (r.nextInt(26) + 'a');
            name += c;
        }
        return name;
    }
}
