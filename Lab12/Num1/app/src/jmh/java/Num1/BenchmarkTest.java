package Num1;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Level;
import java.util.Map;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.ArrayList;

@State(Scope.Benchmark)
public class BenchmarkTest {

    Maps mapOperations;

    Map<String, Integer> map;
    HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
    LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<String, Integer>();
    TreeMap<String, Integer> treeMap = new TreeMap<String, Integer>();
    Hashtable<String, Integer> hashTable = new Hashtable<String, Integer>();
    ArrayList<Integer> arrayListHashMap = new ArrayList<Integer>();
    ArrayList<Integer> arrayListLinkedHashMap = new ArrayList<Integer>();
    ArrayList<Integer> arrayListTreeMap = new ArrayList<Integer>();
    ArrayList<Integer> arrayListHashTable = new ArrayList<Integer>();


    public BenchmarkTest() {
        mapOperations = new Maps();
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationHashMap() {
        map = mapOperations.generationMap(hashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testPutListHashMap() {
        arrayListHashMap = mapOperations.putList(map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSortingListHashMap() {
        arrayListHashMap = mapOperations.sortingList(arrayListHashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationLinkedHashMap() {
        map = mapOperations.generationMap(linkedHashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testPutListLinkedHashMap() {
        arrayListLinkedHashMap = mapOperations.putList(map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSortingListLinkedHashMap() {
        arrayListLinkedHashMap = mapOperations.sortingList(arrayListLinkedHashMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationTreeMap() {
        map = mapOperations.generationMap(treeMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testPutListTreeMap() {
        arrayListTreeMap = mapOperations.putList(map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSortingListTreeMap() {
        arrayListTreeMap = mapOperations.sortingList(arrayListTreeMap);
    }

    @Setup(Level.Trial) @Benchmark
    public void testGenerationHashTable() {
        map = mapOperations.generationMap(hashTable);
    }

    @Setup(Level.Trial) @Benchmark
    public void testPutListHashTable() {
        arrayListHashTable = mapOperations.putList(map);
    }

    @Setup(Level.Trial) @Benchmark
    public void testSortingListHashTable() {
        arrayListHashTable = mapOperations.sortingList(arrayListHashTable);
    }
}
