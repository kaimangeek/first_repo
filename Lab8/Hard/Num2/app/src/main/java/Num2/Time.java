package Num2;

public class Time {
    long timeout;
    double[] massive;
    int x;
    long spentTime;

    Time(long timeout, int x) {
        this.timeout = timeout;
        this.x = x;
    }

    boolean success() {
        return spentTime < timeout;
    }

    public double[] getMassive() {
        if (success()) {
            return this.massive;
        }
        return null;
    }

    void generate() {
        massive = new double[x];
        long time = System.currentTimeMillis();
        for (int i = 0; i < x; i++) {
            massive[i] = Math.random();
        }
        this.spentTime = System.currentTimeMillis() - time;
    }

}
