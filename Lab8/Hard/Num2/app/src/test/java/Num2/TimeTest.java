package Num2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TimeTest {
    @Test
    void generatorTimeError() {
        Time Time = new Time(10, 100000);
        Time.generate();
        assertFalse(Time.success());
    }

    @Test
    void generatorSuccess() {
        Time Time = new Time(1000, 100);
        Time.generate();
        assertTrue(Time.success());
    }

    @Test
    void generatorArrayNotEmpty() {
        Time Time = new Time(1000, 100);
        Time.generate();
        assertNotNull(Time.getMassive());
    }

    @Test
    void generatorTimeErrorArray() {
        Time Time = new Time(10, 100000);
        Time.generate();
        assertNull(Time.getMassive());
    }
}
