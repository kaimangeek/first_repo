package Num5;

import java.util.Scanner;

import static java.lang.Math.abs;

public class Shift {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int range = sc.nextInt();
        int shift = sc.nextInt();

        Seg[] segments = new Seg[x];

        int max = shift + range;
        int min = abs(range - shift);
        int crossings = 0;

        for (int i = 0; i < x; i++) {
            segments[i] = new Seg();
            segments[i].point1 = (int) (Math.random() * max) + min;
            segments[i].point2 = (int) (Math.random() * max) + min;
            segments[i].point3 = (int) (Math.random() * max) + min;
            segments[i].point4 = (int) (Math.random() * max) + min;
        }

        for (int i = 0; i < x; i++) {
            if (((segments[i].point1 > segments[i].point3 && segments[i].point1 < segments[i].point4) ||
                    (segments[i].point2 > segments[i].point3 && segments[i].point2 < segments[i].point4)) ||
                    (segments[i].point1 < segments[i].point3 && segments[i].point2 > segments[i].point4) ||
                    ((segments[i].point1 > segments[i].point4 && segments[i].point1 < segments[i].point3) ||
                            (segments[i].point2 > segments[i].point4 && segments[i].point2 < segments[i].point3)) ||
                            (segments[i].point1 < segments[i].point4 && segments[i].point2 > segments[i].point3)) {
                crossings++;

            }
        }
        System.out.println(crossings);

    }
}
