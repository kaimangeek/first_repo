package Num6;

import java.util.Scanner;

class Mat {
    public static int[][][] setMatrix(int x, int y) {
        int[][][] matrix = new int[x][y][y];
        for (int d = 0; d < x; d++) {
            for (int i = 0; i < y; i++) {
                for (int j = 0; j < y; j++) {
                    matrix[d][i][j] = (int) (Math.random() * 10);
                }
            }
        }
        return matrix;
    }

    public static int[][] multiplyMatrix(int[][] matrix1, int[][] matrix2) throws NullPointerException, MatException {
        if (matrix1 == null || matrix2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (matrix1[0].length != matrix2.length) {
            throw new MatException("Нельзя умножать такие матрицы");
        }
        int[][] thrArray = new int[matrix1.length][matrix2.length];
        int sum = 0;
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[i].length; j++) {
                for (int y = 0; y < matrix2[j].length; y++) {
                    sum += matrix1[y][j];
                }
                thrArray[i][j] = sum;
            }
        }
        return thrArray;
    }


    public static void main(String[] args) throws MatException {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        int width = sc.nextInt();
        long timeStart = System.currentTimeMillis();
        int[][][] st = setMatrix(count, width);
        for (int i = 0; i < count - 1; i++) {
            st[i + 1] = multiplyMatrix(st[i], st[i + 1]);
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(st[st.length - 1][i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(System.currentTimeMillis() - timeStart + "мс");

    }
}
