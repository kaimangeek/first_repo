package Num6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MatTest {
    @Test
    void matrixMultiply() {
        int[][] matBase = {{10,4,5},{8,5,4}};
        int[][] matMultiplier = {{-3,11},{7,-6},{4,5}};
        int[][] matExpected = {{18,111}, {27,78}};
        assertArrayEquals(matExpected, Mat.multiplyMatrix(matBase, matMultiplier));
    }
    @Test
    void matrixMultiplyOne() {
        int[][] matBase = {{1}};
        int[][] matMultiplier = {{1}};
        int[][] matExpected = {{1}};
        assertArrayEquals(matExpected, Mat.multiplyMatrix(matBase, matMultiplier));
    }
    @Test
    void matrixMultiplyBlank() {
        int[][] matBase = {{0}};
        int[][] matMultiplier = {{0}};
        int[][] matExpected = {{0}};
        assertArrayEquals(matExpected, Mat.multiplyMatrix(matBase, matMultiplier));
    }

}
