package Num1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StrIntT {
    @Test
    void toStringTest() {
        StrInt st = new StrInt();
        String b = "123";
        assertEquals(b, st.tostr("+1"));
    }

    @Test
    void BlankTest() {
        StrInt st = new StrInt();
        String b = "";
        assertEquals(b, st.tostr(""));
    }

    @Test
    void negativeTest() {
        StrInt st = new StrInt();
        String b = "-123";
        assertEquals(b, st.tostr("-1"));
    }

}
