package Num4;

public class Experiment {

    long time = 0;
    double number = 0;

    void  process(double numberThreshold, double coefficient, int maxTime) {

        double max = coefficient * numberThreshold;
        double min = -max;

        long timeStart = System.currentTimeMillis();
        while (maxTime > System.currentTimeMillis() - timeStart) {
            try {
                Thread.sleep((int) Math.random() * (100) + 900);
                double randomNumber = (Math.random() * ((max - min) + 1)) + min;
                if (randomNumber < numberThreshold) {
                    this.time = System.currentTimeMillis() - timeStart;
                    this.number = randomNumber;
                } else {
                    break;
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
