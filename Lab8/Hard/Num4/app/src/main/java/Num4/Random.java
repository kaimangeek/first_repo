package Num4;

import java.util.Scanner;

public class Random {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double numberThreshold = sc.nextDouble();
        double coefficient = sc.nextDouble();
        int max_time = sc.nextInt();

        Experiment exp = new Experiment();
        exp.process(numberThreshold, coefficient, max_time);
        System.out.println(exp.time + ": " + exp.number);

    }
}

