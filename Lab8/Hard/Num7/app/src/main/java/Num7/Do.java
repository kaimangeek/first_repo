package Num7;

abstract class Do {
    String message;

    public Do(String message){
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }

    abstract public void doSmth() throws Exception;
}
