package Num7;

import java.util.*;

public class Do3 extends Do {

    public Do3(String message) {
        super(message);
    }

    public void doSmth() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число: ");
        long a = sc.nextInt();
        System.out.println("Квадрат числа равен: " + (a * a));
    }
}
