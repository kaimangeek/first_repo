package Num7;

import java.util.*;

public class Menu {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int userRequest = 0;
        Unit begin = new Unit();
        Unit level1 = new Unit();
        Unit level2 = new Unit();
        begin.setChild(level1);
        begin.setDoing(new Do1("Умножение"));
        level1.setDoing(new Do2("Целочисленное деление"));
        level1.setChild(level2);
        level2.setDoing(new Do3("Возведение в квадрат"));

        while (true){
            begin.output();
            userRequest = sc.nextInt();
            System.out.println();
            if (userRequest == 0){
                if (begin.getParent() == null){
                    break;
                }
                else{
                    begin = begin.getParent();
                }
            }
            if (userRequest > begin.sizeChild() && userRequest <= begin.sizeDoing() + begin.sizeChild()){
                begin.doing.get(userRequest - begin.sizeChild() - 1).doSmth();
            }
            if (userRequest > 0 && userRequest <= begin.sizeChild()){
                begin = begin.child.get(userRequest - 1);
            }
        }

    }
}
