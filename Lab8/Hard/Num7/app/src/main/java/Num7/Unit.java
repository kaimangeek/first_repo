package Num7;

import java.util.*;

public class Unit {
    public ArrayList<Unit> child = new ArrayList<Unit>();
    public ArrayList<Do> doing = new ArrayList<Do>();
    private Unit parent;

    public void setParent(Unit parent){
        this.parent = parent;
    }


    public Unit getParent(){
        return this.parent;
    }

    public void setDoing(Do action){
        this.doing.add(action);
    }

    public void setChild(Unit child){
        this.child.add(child);
        child.parent = this;
    }

    public int sizeChild(){
        return this.child.size();
    }

    public int sizeDoing(){
        return this.doing.size();
    }

    public void output(){
        System.out.println("Приветствую! ");
        if (this.parent == null){
            System.out.println("0. Выход");
        }
        else{
            System.out.println("0. Вернуться");
        }
        int level = 1;
        for (int i = 0; i < sizeChild(); i++){
            System.out.println(level + ". " + "Следующий уровень");
            level++;
        }

        for (int i = 0; i < sizeDoing(); i++){
            System.out.println(level + ". " + this.doing.get(i).getMessage());
            level++;
        }
    }
}
