package Num3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SortTest {
    @Test
    void doubleMassSort() {
        double[] a = {5, 4, 7, 3, 1, 10, 2};
        DubleSort.sort(a);
        double[] b = {1, 2, 3, 4, 5, 7, 10};
        assertArrayEquals(b, a);
    }

    @Test
    void doubleMassSortOne() {
        double[] a = {1};
        DubleSort.sort(a);
        double[] b = {1};
        assertArrayEquals(b, a);
    }

    @Test
    void doubleMassSortBlank() {
        double[] a = {};
        DubleSort.sort(a);
        double[] b = {};
        assertArrayEquals(b, a);
    }

    @Test
    void createSortedArray() {
        double[] a = {5, 4, 7, 3, 1, 10, 2};
        double[] b = {1, 2, 3, 4, 5, 7, 10};
        assertArrayEquals(b, DubleSort.createSortedArray(a));
    }


    @Test
    void createSortedArrayOne() {
        double[] a = {1};
        double[] b = {1};
        assertArrayEquals(b, DubleSort.createSortedArray(a));
    }


    @Test
    void createSortedArrayBlank() {
        double[] a = {};
        double[] b = {};
        assertArrayEquals(b, DubleSort.createSortedArray(a));
    }


}
