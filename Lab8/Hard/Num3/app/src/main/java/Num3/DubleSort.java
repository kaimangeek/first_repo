package Num3;

public class DubleSort {
    static void sort(double[] mass) {
        for (int i = 0; i < mass.length - 1; i++) {
            for (int j = 0; j < mass.length - i - 1; j++) {
                if (mass[j] > mass[j + 1]) {
                    double buff = mass[j];
                    mass[j] = mass[j + 1];
                    mass[j + 1] = buff;
                }
            }
        }
    }

    static double[] createSortedArray(double[] mass) {
        double[] newMass = mass.clone();
        sort(newMass);
        return newMass;
    }
}
