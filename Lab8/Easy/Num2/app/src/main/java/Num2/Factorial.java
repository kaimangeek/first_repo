package Num2;

public class Factorial {
    static long factorial(int max){
        long x = 1;
        for (int i = 1; i <= max; i++) {
            x = x * i;
        }

        return x;
    }
}
