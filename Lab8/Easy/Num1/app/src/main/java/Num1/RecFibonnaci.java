package Num1;

import java.util.*;
import java.lang.*;

public class RecFibonnaci {
    public static long fib(int a, long[] fibo) {
        if (fibo[a - 1] != 0) {
            return fibo[a - 1];
        }
        if (a <= 2) {
            fibo[a - 1] = a - 1;
            return fibo[a - 1];
        }
        long res = fib(a - 1, fibo) + fib(a - 2, fibo);
        fibo[a - 1] = res;
        return res;
    }

    public static long[] getFib(int n) {
        if (n == 0) {
            return null;
        }
        long[] fibo_new = new long[n];
        fib(n, fibo_new);
        return fibo_new;
    }

    public static void main(String[] args) {
        int n = 10;
        System.out.println(Arrays.toString(getFib(n)));
    }
}
