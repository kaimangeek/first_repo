package Num1;

class Fibonnaci {
    static long[] fiboline(int a) {
        long x = 0;
        long y = 1;
        long end = a;
        long[] arr = new long[a];
        if (end == 1) {
            arr[0] = x;
        } else if (end > 1) {
            arr[0] = x;
            arr[1] = y;
        }

        for (int i = 0; i < end - 2; i++) {
            long res = x + y;
            arr[i + 2] = res;
            x = y;
            y = res;
        }

        return arr;
    }
}
