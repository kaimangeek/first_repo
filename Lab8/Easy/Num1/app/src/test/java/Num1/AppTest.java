package Num1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FibTest {
    @Test
    void checkFib() {
        int a = 10;
        long[] b = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        assertArrayEquals(b, RecFibonnaci.getFib(a));
    }

    @Test
    void checkOneFib() {
        int a = 1;
        long[] b = {0};
        assertArrayEquals(b, Fibonnaci.fiboline(a));
    }

    @Test
    void checkEmptyFib() {
        int a = 0;
        assertArrayEquals(null, RecFibonnaci.getFib(a));
    }
}

