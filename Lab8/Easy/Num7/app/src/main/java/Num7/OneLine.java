package Num7;

public class OneLine {
    static String toLine(int[][] matrix) {
        String x = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                x = x + matrix[i][j] + " ";
            }
        }
        return x;
    }
}
