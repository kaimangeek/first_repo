package Num5;

public class Counter {
    static int count(String str, char sym) {
        char[] array_str = str.toCharArray();
        int sum = 0;
        for (int i = 0; i < array_str.length; i++) {
            if (array_str[i] == sym) {
                sum++;
            }
        }
        return sum;
    }
}
