package binarytree2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class Tests<T> {

    private Tree<Integer> targetTree;
    private Node<Integer> node = new Node<Integer>(10, null, null);

    @BeforeEach
    public void init() {
        targetTree = new Tree<>();
    }

    @Test
    public void setLeftOperandTest() {
        targetTree.setLeft(new Node<>(20, null, null));
        Assertions.assertTrue(targetTree.contains(20));
    }

    @Test
    public void setRightOperandTest() {
        targetTree.setRight(new Node<>(25, null, null));
        Assertions.assertTrue(targetTree.contains(25));
    }

    @Test
    public void setValueTest() {
        targetTree.setValue(30);
        Assertions.assertTrue(targetTree.contains(30));
    }

    @Test
    public void getLeftOperandTest() {
        targetTree.setLeft(new Node<>(35, null, null));
        Node<Integer> node = targetTree.getLeft();
        Assertions.assertEquals(35, node.getValue());
    }

    @Test
    public void getRightOperandTest() {
        targetTree.setRight(new Node<>(40, null, null));
        Node<Integer> node = targetTree.getRight();
        Assertions.assertEquals(40, node.getValue());
    }

    @Test
    public void getValueTest() {
        targetTree.setValue(45);
        Assertions.assertEquals(45, targetTree.getValue());
    }

    @Test
    public void addIntegerAndContainsTest() {
        targetTree.add(50);
        Assertions.assertTrue(targetTree.contains(50));
    }

    @Test
    public void removeIntegerTest() {
        targetTree.add(55);
        Assertions.assertTrue(targetTree.contains(55));
        targetTree.remove(55);
        Assertions.assertFalse(targetTree.contains(55));
    }

    @Test
    public void containsTest() {
        targetTree.add(60);
        Assertions.assertTrue(targetTree.contains(60));
    }

    @Test
    public void sizeTest() {
        targetTree.add(70);
        Assertions.assertEquals(1, targetTree.size());
    }

    @Test
    public void clearAndIsEmptyTest() {
        targetTree.add(75);
        targetTree.clear();
        Assertions.assertTrue(targetTree.isEmpty());
    }

    @Test
    public void toArrayTest() {
        targetTree.add(80);
        targetTree.add(85);
        targetTree.add(90);
        assertArrayEquals(new Object[]{80}, targetTree.toArray());
    }
    @Test
    public void iteratorTest1() {
        targetTree.add(1);
        targetTree.add(5);
        targetTree.setRight(node);
        System.out.println(targetTree.getLeft());
        int[] arr = new int[targetTree.size()];
        int[] nodesArr = {5, 1, 10};
        int i = 0;
          while (targetTree.iterator().hasNext()) {
              arr[i] = (targetTree.iterator().next());
              i++;
          }
          assertArrayEquals(nodesArr, arr);
    }
    @Test
    public void removeAllAndContainsAllTest() {
        Collection<Integer> list = Arrays.asList(80, 90);
        targetTree.removeAll(list);
        Assertions.assertFalse(targetTree.containsAll(list));
    }

}
