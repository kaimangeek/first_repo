package binarytree2;

import java.util.Collection;
import java.util.Iterator;

public class Tree<T> implements Collection<T> {
    private Node<T> rootNode;
    private int index = 0;

    @Override
    public int size() {
        return calculateSize(rootNode);
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return containsNodeRec((T) o, rootNode);
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            final Object[] values = toArray();
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < values.length;
            }
            @Override
            public T next() {
                int oldIndex = index;
                index++;
                return (T) values[oldIndex];
            }

        };

    }

    @Override
    public Object[] toArray() {
        Object[] values = new Object[calculateSize(rootNode)];
        index = 0;
        preOrderTravers(rootNode, values);
        return values;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    private void preOrderTravers(Node<T> head, Object[] values) {
        if (head != null) {
            values[index] = head.getValue();
            index++;
            preOrderTravers(head.getLeft(), values);
            preOrderTravers(head.getRight(), values);
        }
    }

    @Override
    public boolean add(T t) {
        if (contains(t)) return false;
        rootNode = addRec(t, rootNode);
        return true;
    }

    private Node<T> addRec(T t, Node<T> rootNode) {
        if (rootNode == null) {
            return new Node<>(t, null, null);
        }
        if (getRight() != null) {
            rootNode.setLeft(addRec(t, rootNode.getLeft()));
            return rootNode;
        }
        else {
            rootNode.setRight(addRec(t, rootNode.getRight()));
            return rootNode;
        }
    }

    @Override
    public boolean remove(Object o) {
        if (!contains(o)) return false;
        rootNode = deleteRec((T) o, rootNode);
        return true;
    }

    private Node<T> deleteRec(T t, Node<T> rootNode) {
        if (rootNode == null)
            return null;
        if (rootNode.getValue().equals(t)) {
            if (rootNode.getLeft() == null && rootNode.getRight() == null) {
                return null;
            }
            if (rootNode.getLeft() == null) {
                return rootNode.getRight();
            }
            if (rootNode.getRight() == null) {
                return rootNode.getLeft();
            }
        }
        if (containsNodeRec(t, rootNode.getLeft())) {
            rootNode.setLeft(deleteRec(t, rootNode.getLeft()));
        } else {
            rootNode.setRight(deleteRec(t, rootNode.getRight()));
        }
        return rootNode;
    }

    private boolean containsNodeRec(T t, Node<T> rootNode) {
        if (rootNode == null) {
            return false;
        }
        if (t == rootNode.getValue()) {
            return true;
        }
        return containsNodeRec(t, rootNode.getRight()) || containsNodeRec(t, rootNode.getLeft());
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c)
            if (!contains(o))
                return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T o : c)
            add(o);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c)
            remove(o);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        rootNode = null;
    }


    public T getValue() {
        if (rootNode == null)
            return null;
        return rootNode.getValue();
    }

    private int calculateSize(Node<T> node) {
        if (node == null)
            return 0;
        return 1 + calculateSize(node.getRight()) + calculateSize(node.getLeft());
    }


    public Node<T> getLeft() {
        if (rootNode == null)
            return null;
        return rootNode.getLeft();
    }


    public Node<T> getRight() {
        if (rootNode == null)
            return null;
        return rootNode.getRight();
    }


    public void setValue(T value) {
        if (rootNode == null)
            rootNode = new Node<>(value, null, null);
        else {
            rootNode.setValue(value);
        }
    }


    public void setLeft(Node<T> node) {
        if (rootNode == null) {
            rootNode = new Node<>(null, node, null);
        } else {
            rootNode.setLeft(node);
        }
    }

    public void setRight(Node<T> node) {
        if (rootNode == null) {
            rootNode = new Node<>(null, null, node);
        } else {
            rootNode.setRight(node);
        }
    }
}
