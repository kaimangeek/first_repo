package binarytree2;

import java.util.Collection;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        Tree<Integer> tree = new Tree<>();
        tree.add(100);
        tree.add(50);
        tree.add(60);
        tree.remove(60);
        tree.setRight(new Node<>(20, null, null));
        printTree(tree);
    }

    private static void printTree(Collection<Integer> tree) {
        Iterator<Integer> treeIterator = tree.iterator();
        System.out.print("Values: ");
        while (treeIterator.hasNext())
            System.out.print(" " + treeIterator.next());
        System.out.println();
    }
}
