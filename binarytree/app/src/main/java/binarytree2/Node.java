package binarytree2;

public class Node<T> {

    private T value;
    private Node<T> left;
    private Node<T> right;

    public Node(T value, Node<T> left, Node<T> right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public static <T> Node<T> empty() {
        return new Node<>(null, null, null);
    }


    public T getValue() {
        return value;
    }


    public Node<T> getLeft() {
        return left;
    }


    public Node<T> getRight() {
        return right;
    }


    public void setValue(T value) {
        this.value = value;
    }


    public void setLeft(Node<T> node) {
        this.left = left;
    }


    public void setRight(Node<T> node) {
        this.right = right;
    }

}

