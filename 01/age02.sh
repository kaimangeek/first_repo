#!/bin/bash
s=1
step1=$((2**18))
step2="$(date)"
while [ $s -lt $step1 ]
do
 ((s++))
 echo $((2 ** $s))
done
echo "$step2"
echo "$(date)"
