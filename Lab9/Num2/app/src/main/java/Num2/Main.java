package Num2;

public class Main {
    public static void main(String[] args) {
        Vec a = new Vec(3, 4);
        Vec b = new Vec(2, 2);
        Vec c = Vec.sub(a, b);
        System.out.println(c);

        Vec q = Vec.mul(a, 3);
        System.out.println(q);

        Vec w = Vec.sum(a, b);
        System.out.println(w);

        Vec e = Vec.div(b, 2);
        System.out.println(e);
    }

}
