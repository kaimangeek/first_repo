package Num2;

public class Vec {
    private double x;
    private double y;

    public Vec(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vec() {
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String toString() {
        return "Координаты вектора (x: " + this.x + "; " + "y: " + this.y + ")";
    }

    public static Vec sum(Vec v1, Vec v2) {
        Vec res = new Vec();
        res.x = v1.x + v2.x;
        res.y = v1.y + v2.y;
        return res;
    }

    public static Vec sub(Vec v1, Vec v2) {
        Vec res = new Vec();
        res.x = v1.x - v2.x;
        res.y = v1.y - v2.y;
        return res;
    }

    public static Vec mul(Vec v1, double a) {
        v1.x *= a;
        v1.y *= a;
        return v1;
    }

    public static Vec div(Vec v1, double a) {
        v1.x /= a;
        v1.y /= a;
        return v1;
    }
}
