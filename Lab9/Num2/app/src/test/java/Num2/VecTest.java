package Num2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VecTest {
    @Test
    void getterTest() {
        Vec a = new Vec(2, 2);
        assertEquals(2, a.getX());
    }

    @Test
    void divisionTest() {
        Vec a = new Vec(2, 2);
        Vec.div(a, 2);
        assertEquals(1, a.getX());
        assertEquals(1, a.getY());
    }

    @Test
    void toStringTest() {
        Vec a = new Vec(2, 1);
        assertEquals("Координаты вектора (x: 2.0; y: 1.0)", a.toString());
    }

    @Test
    void sumTest() {
        Vec a = new Vec(4, 2);
        Vec b = new Vec(4, 1);
        Vec c = Vec.sum(a, b);
        assertEquals(8, c.getX());
        assertEquals(3, c.getY());
    }

    @Test
    void multiplyTest() {
        Vec a = new Vec(2, 2);
        Vec.mul(a, 2);
        assertEquals(4, a.getX());
        assertEquals(4, a.getY());
    }

    @Test
    void subtractionTest() {
        Vec a = new Vec(4, 3);
        Vec b = new Vec(8, 4);
        Vec c = Vec.sub(a, b);
        assertEquals(-4, c.getX());
        assertEquals(-1, c.getY());
    }
}
