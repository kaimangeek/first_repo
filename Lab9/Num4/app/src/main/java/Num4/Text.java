package Num4;

public class Text implements Inter {
    public String text;

    Text(String text) {
        this.text = text;
    }

    public void output() {
        System.out.println(this);
    }

    public String toString() {
        return text;
    }
}
