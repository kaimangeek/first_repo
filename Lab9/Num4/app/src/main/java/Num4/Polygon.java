package Num4;

public class Polygon implements Inter {
    protected int perimeter;
    protected int[] sides;

    Polygon(int[] sides) throws PolygonException {
        this.sides = sides;
        this.checkPolygonCorrect();
        this.perimeter = polygonPerimeter();
    }

    public int polygonPerimeter() {
        int perimeter = 0;
        for (int i = 0; i < this.sides.length; i++) {
            perimeter += this.sides[i];
        }
        return perimeter;
    }

    public void checkPolygonCorrect(){
        int sum = 0;
        for (int i = 0; i < this.sides.length; i++){
            sum += this.sides[i];
        }
        for (int i = 0; i < this.sides.length; i++){
            if (2 * this.sides[i] > sum){
                try {
                    throw new PolygonException("Неправильный многоугольник");
                } catch (PolygonException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public String toString() {
        return "Это " + this.sides.length + "-угольник. Периметр: " + this.perimeter;
    }

    public void output() {
        System.out.println(this);
    }
}
