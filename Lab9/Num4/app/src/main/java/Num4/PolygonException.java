package Num4;

public class PolygonException extends Exception {
    PolygonException(String message){
        super(message);
    }
}
