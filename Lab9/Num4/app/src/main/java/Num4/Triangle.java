package Num4;

public class Triangle extends Polygon {
    Triangle(int[] sides) throws PolygonException {
        super(sides);
        if (sides.length != 3) {
            this.sides = null;
            this.perimeter = 0;
            throw new PolygonException("Не треугольник");
        }
    }

    public String toString() {
        if (this.perimeter == 0) {
            return "Это не треугольник";
        }
        return "Это треугольник. Периметр: " + this.perimeter;
    }

    public void output() {
        System.out.println(this);
    }
}
