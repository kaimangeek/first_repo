package Num4;

public class Main {
    public static void main(String[] args) throws PolygonException {

        int[] sidesA = new int[]{3, 4, 5};
        Triangle triangleA = new Triangle(sidesA);

        int[] sidesB = new int[]{1, 2, 3};
        Triangle triangleB = new Triangle(sidesB);

        int[] sidesC = new int[]{5, 5, 5};
        Triangle triangleC = new Triangle(sidesC);

        int[] sidesD = new int[]{2345, 23456, 234567};
        Polygon polygon = new Polygon(sidesD);

        Text textOne = new Text("smth");
        Text textTwo = new Text("smth more");

        Inter[] in = new Inter[]{triangleA, triangleB, triangleC, polygon, textOne, textTwo};
        Oper operation = new Oper(in);
        operation.output();
    }
}
