package Num4;

public class Oper implements Inter {
    public Inter[] allObjects;

    Oper(Inter[] allObjects) {
        this.allObjects = allObjects;
    }

    public void output() {
        for (int i = 0; i < this.allObjects.length; i++) {
            this.allObjects[i].output();
        }
    }
}
