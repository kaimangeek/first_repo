package Num4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TriangleTest {
    @Test
    void trianglePerimeterTest() throws PolygonException {
        int[] a = new int[]{1, 1, 1};
        Triangle b = new Triangle(a);
        assertEquals(3, b.polygonPerimeter());
    }

    @Test
    void toStringTriangle() throws PolygonException {
        int[] a = new int[]{1, 1, 1};
        Triangle b = new Triangle(a);
        assertEquals("Это треугольник. Периметр: 3", b.toString());
    }

    @Test
    void ExceptionTest() throws PolygonException {
        int[] a = new int[]{1, 1};
        PolygonException exception = assertThrows(PolygonException.class, () -> new Triangle(a));
        assertEquals("Это не треугольник", exception.message);
    }

}
