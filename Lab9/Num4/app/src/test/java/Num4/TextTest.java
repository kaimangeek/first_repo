package Num4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TextTest {
    @Test
    void toStringText() {
        Text a = new Text("abcd");
        assertEquals("abcd", a.toString());
    }
}
