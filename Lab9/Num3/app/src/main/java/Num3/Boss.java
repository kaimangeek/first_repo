package Num3;

public class Boss extends Acc {

    Boss(String name, int money) {
        super(name, money);
    }

    public String acc() {
        return "Я занимаюсь важной бухгалтерией";
    }

    public void changeSalary(Worker worker, int money) {
        worker.salary = money;
    }

    public String toString() {
        return "Имя = " + name + ", должность = главный бухгалтер" + ", зарплата = " + salary;
    }
}
