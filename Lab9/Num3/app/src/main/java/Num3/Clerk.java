package Num3;

public class Clerk extends Worker {

    Clerk(String name, int money) {
        super(name, money);
    }

    public String clerk() {
        return "Я ношу бумажки";
    }

    public String toString() {
        return "Имя = " + name + ", должность = клерк" + ", зарплата = " + salary;
    }
}
