package Num3;

public class Eng extends Worker {
    Eng(String name, int money) {
        super(name, money);
    }

    public String engineer() {
        return "Я занимаюсь инженирией";
    }

    public String toString() {
        return "Имя = " + name + ", должность = инженер" + ", зарплата = " + salary;
    }
}
