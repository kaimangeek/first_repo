package Num3;

public class Worker {
    protected String name;
    protected int salary;

    public Worker(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "Имя = " + name + ", должность = сотрудник" + ", зарплата = " + salary;
    }

    public int hashCode() {
        return name.length() * 100 + salary;
    }

    public boolean equals(Object o) {
        super.equals(o);
        Worker worker = (Worker) o;
        if (worker.getName() == this.getName() && worker.getSalary() == this.getSalary()) {
            return true;
        }
        return false;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
