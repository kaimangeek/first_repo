package Num3;

public class Main {
    public static void main(String[] args) {
        Worker[] workers = new Worker[4];

        workers[0] = new Acc("Петр", 25000);
        workers[1] = new Boss("Василий", 300000);
        workers[2] = new Eng("Генадий", 30000);
        workers[3] = new Worker("Александр", 10000);
        for (int i = 0; i < workers.length; i++) {
            System.out.println(workers[i]);
        }
    }
}
