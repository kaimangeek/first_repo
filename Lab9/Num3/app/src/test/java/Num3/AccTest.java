package Num3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccTest {
    @Test
    void acc() {
        Acc accountant = new Acc("Петр", 25000);
        assertEquals("Я занимаю бухгалтерией", accountant.acc());
    }

    @Test
    void accToString() {
        Acc accountant = new Acc("Петр", 25000);
        assertEquals("Имя = Петр, должность = бухгалтер, зарплата = 25000", accountant.toString());
    }
}
