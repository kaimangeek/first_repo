package Num3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EngTest {
    @Test
    void engineer() {
        Eng eng = new Eng("Генадий", 30000);
        assertEquals("Я занимаюсь инженирией", eng.engineer());
    }

    @Test
    void engineerToString() {
        Eng eng = new Eng("Генадий", 30000);
        assertEquals("Имя = Генадий, должность = инженер, зарплата = 30000", eng.toString());
    }
}
