package Num3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class WorkerTest {
    @Test
    void equalsFalse() {
        Worker worker = new Worker("Александр", 10000);
        Eng eng = new Eng("Генадий", 30000);
        assertFalse(worker.equals(eng));
    }

    @Test
    void equalsTrue() {
        Worker worker = new Worker("Александр", 10000);
        Worker worker1 = new Worker("Александр", 10000);
        assertTrue(worker.equals(worker1));
    }

    @Test
    void hashCodeCheck() {
        Worker employee = new Worker("Александр", 10000);
        assertEquals(10800, employee.hashCode());
    }

    @Test
    void cloneCheck() {
        Worker employee = new Worker("Александр", 10000);
        assertNull(employee.clone());
    }

    @Test
    void SalaryCheck() {
        Worker employee = new Worker("Александр", 10000);
        assertEquals(10000, employee.getSalary());
    }
    @Test
    void NameCheck() {
        Worker employee = new Worker("Александр", 10000);
        assertEquals("Александр", employee.getName());
    }
    @Test
    void employeeToString() {
        Worker worker = new Worker("Александр", 10000);
        assertEquals("Имя = Александр, должность = сотрудник, зарплата = 10000", worker.toString());
    }

}
