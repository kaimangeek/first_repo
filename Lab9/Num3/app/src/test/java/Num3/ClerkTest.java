package Num3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ClerkTest {
    @Test
    void clerk() {
        Clerk clerk = new Clerk("Александр", 10000);
        assertEquals("Я ношу бумажки", clerk.clerk());
    }

    @Test
    void workerToString() {
        Clerk clerk = new Clerk("Александр", 10000);
        assertEquals("Имя = Александр, должность = рабочий, зарплата = 10000", clerk.toString());
    }
}
