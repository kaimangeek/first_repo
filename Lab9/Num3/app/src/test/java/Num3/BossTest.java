package Num3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BossTest {
    @Test
    void boss() {
        Boss boss = new Boss("Василий", 300000);
        assertEquals("Я занимаюсь важной бухгалтерией", boss.acc());
    }

    @Test
    void bossToString() {
        Boss boss = new Boss("Василий", 300000);
        assertEquals("Имя = Василий, должность = главный бухгалтер, зарплата = 300000", boss.toString());
    }

    @Test
    void bossChangeSalary() {
        Boss chiefAccountant = new Boss("Василий", 30000);
        Worker worker = new Worker("Александр", 10000);
        chiefAccountant.changeSalary(worker, 5000);
        assertEquals(5000, worker.getSalary());
    }
}
