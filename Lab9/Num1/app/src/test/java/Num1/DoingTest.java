package Num1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DoingTest {
    @Test void tryMultiplication() throws MatException {
        int[][] target = new int[][]{{18,111}, {27,78}};
        Mat m1 = new Mat(2,3);
        int[][] numbers1 = new int[][]{{10,4,5},{8,5,4}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(3,2);
        int [][] numbers2 = new int[][]{{-3,11},{7,-6},{4,5}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.mul(m1, m2), "tryMultiplication is not correct");
    }

    @Test void tryMultiplication4x3And3X2() throws MatException {
        int[][] target = new int[][]{{11,11}, {4,8}, {6,9}, {3,1}};
        Mat m1 = new Mat(4,3);
        int[][] numbers1 = new int[][]{{1,0,2},{0,4,0},{0,2,1},{1,0,0}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(3,2);
        int [][] numbers2 = new int[][]{{3,1},{1,2},{4,5}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.mul(m1, m2), "tryMultiplication3x4And3X2 is not correct");
    }

    @Test void tryAdding() throws MatException {
        int[][] target = new int[][]{{7,15}, {15,-1}};
        Mat m1 = new Mat(2,2);
        int[][] numbers1 = new int[][]{{10,4},{8,5}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(2,2);
        int [][] numbers2 = new int[][]{{-3,11},{7,-6}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.sum(m1, m2), "tryAdding is not correct");
    }

    @Test void trySubstraction() throws MatException {
        int[][] target = new int[][]{{13,-7}, {1,11}};
        Mat m1 = new Mat(2,2);
        int[][] numbers1 = new int[][]{{10,4},{8,5}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(2,2);
        int [][] numbers2 = new int[][]{{-3,11},{7,-6}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.sub(m1, m2), "trySubstraction is not correct");
    }

    @Test void tryZeroMult() throws MatException {
        int[][] target = new int[][]{{0}};
        Mat m1 = new Mat(1,1);
        int[][] numbers1 = new int[][]{{0}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(1,1);
        int [][] numbers2 = new int[][]{{0}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.mul(m1, m2), "tryZeroMult is not correct");
    }
    @Test void tryZeroSum() throws MatException {
        int[][] target = new int[][]{{0}};
        Mat m1 = new Mat(1,1);
        int[][] numbers1 = new int[][]{{0}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(1,1);
        int [][] numbers2 = new int[][]{{0}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.sum(m1, m2), "tryZeroSum is not correct");
    }
    @Test void tryZeroSub() throws MatException {
        int[][] target = new int[][]{{0}};
        Mat m1 = new Mat(1,1);
        int[][] numbers1 = new int[][]{{0}};
        m1.setMatrix(numbers1);
        Mat m2 = new Mat(1,1);
        int [][] numbers2 = new int[][]{{0}};
        m2.setMatrix(numbers2);
        assertArrayEquals(target, Doing.sub(m1, m2), "tryZeroSub is not correct");
    }

    @Test
    void matrixSumLengthException() throws MatException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}, {3, 4}};
        Mat matrix1 = new Mat(3, 2);
        Mat matrix2 = new Mat(2, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        MatException exception = assertThrows(MatException.class, () -> Doing.sum(matrix1, matrix2));
        assertEquals("Матрицы не одинаковые", exception.getMessage());
    }

    @Test
    void matrixSubtractionLengthException() throws MatException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}, {3, 4}};
        Mat matrix1 = new Mat(3, 2);
        Mat matrix2 = new Mat(2, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        MatException exception = assertThrows(MatException.class, () -> Doing.sub(matrix1, matrix2));
        assertEquals("Матрицы не одинаковые", exception.getMessage());
    }

    @Test
    void matrixMultiplicationLengthException() throws MatException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}};
        Mat matrix1 = new Mat(3, 2);
        Mat matrix2 = new Mat(1, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        MatException exception = assertThrows(MatException.class, () -> Doing.mul(matrix1, matrix2));
        assertEquals("Высота первой и ширина второй матриц не совпадают", exception.getMessage());
    }


}
