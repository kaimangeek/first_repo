package Num1;

public class Main {
    public static void main(String[] args) throws MatException {

        Doing doing = new Doing();
        int rows1 = 2;
        int columns1 = 4;
        Mat mtrx1 = new Mat(rows1, columns1);
        for (int i = 0; i < mtrx1.getRows(); i++) {
            for (int j = 0; j < mtrx1.getColumns(); j++) {
                System.out.print(mtrx1.getMat()[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        int rows2 = 2;
        int columns2 = 4;
        Mat mtrx2 = new Mat(rows2, columns2);
        doing.printMatrix(mtrx2);

        System.out.println("Summa: ");
        int[][] sumRes = Doing.sum(mtrx1, mtrx2);
        doing.printMatrix(sumRes);

        System.out.println("Raznost: ");
        int[][] subRes = Doing.sub(mtrx1, mtrx2);
        doing.printMatrix(subRes);

        int rows3 = 4;
        int columns3 = 2;
        Mat mtrx3 = new Mat(rows3, columns3);
        doing.printMatrix(mtrx3);

        System.out.println("Ymnozenie: ");
        int[][] mulRes = Doing.mul(mtrx1, mtrx3);
        doing.printMatrix(mulRes);
    }
}
