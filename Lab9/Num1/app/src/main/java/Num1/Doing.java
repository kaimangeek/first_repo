package Num1;

public class Doing {
    public static int[][] sub(Mat x1, Mat x2) throws NullPointerException, MatException {
        int[][] result;
        int[][] mat1 = x1.getMat();
        int[][] mat2 = x2.getMat();
        if (mat1 == null || mat2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (mat1.length != mat2.length || mat1[0].length != mat2[0].length) {
            throw new  MatException("Матрицы не одинакового размера");
        }
        result = new int[mat1.length][mat1[0].length];
        for (int i = 0; i < mat1.length; i++) {
            for (int j = 0; j < mat1[0].length; j++) {
                result[i][j] = mat1[i][j] - mat2[i][j];
            }
        }
        return result;
    }

    public static int[][] sum(Mat x1, Mat x2) throws NullPointerException, MatException {
        int[][] result;
        int[][] mat1 = x1.getMat();
        int[][] mat2 = x2.getMat();
        if (mat1 == null || mat2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (mat1.length != mat2.length || mat1[0].length != mat2[0].length) {
            throw new  MatException("Матрицы не одинакового размера");
        }
        result = new int[mat1.length][mat1[0].length];
        for (int i = 0; i < mat1.length; i++) {
            for (int j = 0; j < mat1[0].length; j++) {
                result[i][j] = mat1[i][j] + mat2[i][j];
            }
        }
        return result;
    }

    public static int[][] mul(Mat x1, Mat x2) throws NullPointerException, MatException {
        int[][] result;
        int[][] matrix1 = x1.getMat();
        int[][] matrix2 = x2.getMat();
        if (matrix1 == null || matrix2 == null) {
            throw new NullPointerException("Матрицы незаполнены");
        }
        if (matrix1[0].length != matrix2.length) {
            throw new  MatException("Нельзя умножать такие матрицы");
        }
        result = new int[matrix1.length][matrix2[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                for (int k = 0; k < matrix2.length; k++) {
                    result[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        return result;
    }
    public void printMatrix(int[][] mat){
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                System.out.print(mat[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printMatrix(Mat x1){
        int[][] mat = x1.getMat();
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                System.out.print(mat[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
