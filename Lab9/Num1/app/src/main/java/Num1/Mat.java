package Num1;

public class Mat {
    private int[][] matrix;
    private int rows;
    private int columns;

    public Mat(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.matrix = new int[this.rows][this.columns];
        generateMatrix();
    }

    public void setRows(int rows) {
        this.rows = rows;
        this.matrix = new int[this.rows][this.columns];
        generateMatrix();
    }

    public void setColumns(int columns) {
        this.columns = columns;
        this.matrix = new int[this.rows][this.columns];
        generateMatrix();
    }

    public int[][] getMat() {
        return matrix;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void generateMatrix() {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                this.matrix[i][j] = (int) (Math.random() * 10);
            }
        }
    }

    public void setMatrix(int[][] matrix) throws MatException {
        if (matrix.length == this.rows) {
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].length != this.columns) {
                    throw new MatException("No");
                }
            }
        }
        else {
            throw new MatException("No");
        }

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }
}
